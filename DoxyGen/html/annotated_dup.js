var annotated_dup =
[
    [ "Ui", "namespace_ui.html", "namespace_ui" ],
    [ "DBComments", "class_d_b_comments.html", "class_d_b_comments" ],
    [ "DBTable", "class_d_b_table.html", "class_d_b_table" ],
    [ "DBTableEx", "class_d_b_table_ex.html", "class_d_b_table_ex" ],
    [ "DBTool", "class_d_b_tool.html", "class_d_b_tool" ],
    [ "Files", "class_files.html", "class_files" ],
    [ "HelloWorld", "class_hello_world.html", null ],
    [ "Highlighter", "class_highlighter.html", "class_highlighter" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "qt_meta_stringdata_MainWindow_t", "structqt__meta__stringdata___main_window__t.html", "structqt__meta__stringdata___main_window__t" ],
    [ "Ui_MainWindow", "class_ui___main_window.html", "class_ui___main_window" ],
    [ "Widget", "class_widget.html", "class_widget" ]
];