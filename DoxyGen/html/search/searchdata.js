var indexSectionsWithContent =
{
  0: "abcdefghlmnopqrstuw~",
  1: "dfhmquw",
  2: "u",
  3: "dfhmuw",
  4: "abcdefghlmnoprsuw~",
  5: "cdeflmrst",
  6: "q"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros"
};

