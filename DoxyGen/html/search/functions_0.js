var searchData=
[
  ['about',['about',['../class_main_window.html#a7be6a5d98970ac1a6296c6f9aee1e9bb',1,'MainWindow']]],
  ['add_5frow',['add_row',['../class_d_b_table_ex.html#a779c473417f8b9ac464a9a9f7df4a3f1',1,'DBTableEx::add_row(int id, std::string item0, int item1, std::string item2, float item3)'],['../class_d_b_table_ex.html#a779c473417f8b9ac464a9a9f7df4a3f1',1,'DBTableEx::add_row(int id, std::string item0, int item1, std::string item2, float item3)']]],
  ['addcolumn',['addColumn',['../class_d_b_table_ex.html#afc26031a89798c0f037c7fa850f8637c',1,'DBTableEx::addColumn(std::string name, std::string type)'],['../class_d_b_table_ex.html#afc26031a89798c0f037c7fa850f8637c',1,'DBTableEx::addColumn(std::string name, std::string type)']]],
  ['addcomment',['addComment',['../class_d_b_comments.html#a95bb02aea06c0fc89671fee658006674',1,'DBComments::addComment(std::string className, int lineStart, std::string teacherComment, std::string studentCode)'],['../class_d_b_comments.html#a95bb02aea06c0fc89671fee658006674',1,'DBComments::addComment(std::string className, int lineStart, std::string teacherComment, std::string studentCode)']]],
  ['alter',['alter',['../class_d_b_table.html#aa55109dd08a7b5f5e1064e5fd34d41ba',1,'DBTable::alter()'],['../class_d_b_table.html#aa55109dd08a7b5f5e1064e5fd34d41ba',1,'DBTable::alter()']]],
  ['ammendcomment',['ammendComment',['../class_d_b_comments.html#ad421d5b959f0ff9c531cd742140a2dc1',1,'DBComments::ammendComment(std::string className, int lineStart, std::string teacherComment)'],['../class_d_b_comments.html#ad421d5b959f0ff9c531cd742140a2dc1',1,'DBComments::ammendComment(std::string className, int lineStart, std::string teacherComment)']]],
  ['append',['append',['../class_files.html#a304e997e47ccd7cf8af9fa44959ee1f1',1,'Files']]]
];
