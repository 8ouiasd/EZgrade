#-------------------------------------------------
#
# Project created by Xingyuan Guo
#-------------------------------------------------

QT       += core gui sql

LIBS += -l sqlite3

CONFIG += c++11

CXXFLAGS= $(CXXFLAGS) -Wno-self-assign

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MasterGUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


#May fail to add line numbers to the export document,
#shift to reference specific code instead.

SOURCES += main.cpp\
        mastergui.cpp \
    highlighter.cpp \

HEADERS  += mastergui.h \
    highlighter.h \

FORMS    += mastergui.ui

INCLUDEPATH += .

RESOURCES += \
    qresource.qrc


HEADERS += ../DatabaseManager/dbcomments.h
HEADERS += ../DatabaseManager/dbrubrics.h
HEADERS += ../DatabaseManager/dbsavedcomments.h
HEADERS += ../DatabaseManager/dbtable.h
HEADERS += ../DatabaseManager/dbtool.h
HEADERS += ../DatabaseManager/dbtoplevel.h
HEADERS += ../DatabaseManager/searchtable.h
HEADERS += ../DatabaseManager/dbstudentgrades.h

SOURCES += ../DatabaseManager/dbcomments.cpp
SOURCES += ../DatabaseManager/dbrubrics.cpp
SOURCES += ../DatabaseManager/dbsavedcomments.cpp
SOURCES += ../DatabaseManager/dbtable.cpp
SOURCES += ../DatabaseManager/dbtool.cpp
SOURCES += ../DatabaseManager/dbtoplevel.cpp
SOURCES += ../DatabaseManager/searchtable.cpp
SOURCES += ../DatabaseManager/dbstudentgrades.cpp
