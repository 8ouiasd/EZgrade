/**
* @author Xingyuan Guo
* @brief
*/

#ifndef MASTERGUI_H
#define MASTERGUI_H
//======================

#include "highlighter.h"
#include "../DatabaseManager/dbtoplevel.h"
#include "../DatabaseManager/searchtable.h"
#include "../DatabaseManager/dbcomments.h"
#include "../DatabaseManager/dbrubrics.h"
#include "../DatabaseManager/dbsavedcomments.h"
#include "../DatabaseManager/dbstudentgrades.h"


#include <iostream>
#include <QMainWindow>
#include <QInputDialog> //Open a QInputDialog to get user input
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QDir>      //http://doc.qt.io/qt-4.8/qdir.html
#include <iostream>
#include <QFileSystemModel>
#include <QClipboard>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include <QDialog>
#include <QtCore>
#include <QtGui>


//======================
namespace Ui {
class MasterGUI;
}

class MasterGUI : public QMainWindow
{
    Q_OBJECT

public:

    explicit MasterGUI(QWidget *parent = 0);
    ~MasterGUI();


private slots:

    void on_specifyPath_clicked();

    void on_fileTreeView_clicked(const QModelIndex &index);

    void on_fileListView_doubleClicked(const QModelIndex &index);

    void on_searchPushButton_clicked();

    void on_javaViewer_selectionChanged();

    void on_showPathPushBotton_clicked();

    void on_chooseDBLocationPB_clicked();

    void on_getClassNamePB_clicked();

    void on_DBLocationPB_clicked();

    void on_assignGradePB_clicked();

    void on_addCommentPB_clicked();

    void on_findSavedCommentsPB_clicked();

    void on_outFileGeneratePB_clicked();

    void on_dbTablesComboBox_currentTextChanged(const QString &arg1);

private:

    QVector<QString> toQStringVector(QVector<std::string> aVector);

    Ui::MasterGUI *ui;

    QString userInputFilePath;
    QFileSystemModel *fileModel;
    QFileSystemModel *viewModel;
    Highlighter *javaHighliter;

    bool searchIsFirstTime;

    QSqlDatabase editableDB;
    QSqlTableModel *editableQtTableModel;
    QString userInputDBPath;

    DBTopLevel *dbTOPLEVELObj;
    std::string dbLocation;

    bool ifDBLocationPB_clicked;

    std::string studentName;
    QString currentClassName;
    std::string currentLabName;

};

//======================
#endif // MASTERGUI_H
