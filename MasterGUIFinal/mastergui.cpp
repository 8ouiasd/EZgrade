/**
* @author Xingyuan Guo
* @brief This is the central class that runs the whole GUI, most of
* functions and does integration with DB.
*/

//========================================
#include "mastergui.h"
#include "ui_mastergui.h"
#include "qtextedithighlighter.h"
#include "linenumberarea.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include <iostream>
#include <QString>
#include <QVector>


//========================================
/**
 * @brief MasterGUI::MasterGUI This constructor is responsible for building the
 * initial GUI setup
 * @param parent inherits QWidget functionality
 * @return nothing
 */
MasterGUI::MasterGUI(QWidget *parent) :
    QMainWindow(parent),

    ui(new Ui::MasterGUI) {

    ui->setupUi(this);

    //Used for the search box within the JavaView (QTextEdit object)
    searchIsFirstTime = true;

}


//========================================
/**
 * @brief MasterGUI::~MasterGUI This destructor releases memory for those
 * finished objects.
 * @return nothing
 */
MasterGUI::~MasterGUI()
{
    delete ui;
    delete fileModel;
    delete viewModel;
    delete javaHighliter;
    delete editableQtTableModel;
    delete dbTOPLEVELObj;
}




//========================================
/**
 * @brief MasterGUI::on_specifyPath_clicked Allows the user
 * to use the button labeled with "Root" on the upper left corner
 * of the UI to specify a root path he/she wants to open in a view
 * like a file browser. Then show all the files under that directory
 * with a filter to only allow Java files to be opened.
 * @return nothing
 */
void MasterGUI::on_specifyPath_clicked()
{
    //The "Root" button
    userInputFilePath =
            QInputDialog::getText(this,
                                  "Path",
                                  "Input a directory path you want to show:");

    //Initiate a QFileSystemModel for the leftmost panel
    fileModel = new QFileSystemModel(this);

    QStringList javaFilter;
    javaFilter << "*.java" << "*.txt";

    fileModel->setFilter(QDir::NoDotAndDotDot | QDir::Dirs);

    fileModel->setNameFilterDisables(false);

    if(userInputFilePath.isNull())
    {
        QDir tempRootDir = QDir::currentPath();
        tempRootDir.cdUp(); tempRootDir.cdUp(); tempRootDir.cdUp();
        userInputFilePath = (tempRootDir.canonicalPath());


    } else {
        userInputFilePath = (userInputFilePath);
    }

    //ui->showUserInputPath->setText(userInputFilePath);


    ui->fileTreeView->setAnimated(true);
    ui->fileTreeView->setIndentation(20);
    ui->fileTreeView->setSortingEnabled(false);

    ui->fileTreeView->setColumnHidden(3,true);

    ui->fileTreeView->setModel(fileModel);
    ui->fileTreeView->setRootIndex(fileModel->setRootPath(userInputFilePath));
    ui->fileTreeView->hideColumn(3); //get rid of the right 3 columns, not useful
    ui->fileTreeView->hideColumn(2);
    ui->fileTreeView->hideColumn(1);


    viewModel = new QFileSystemModel(this);
    viewModel->setFilter(QDir::NoDotAndDotDot | QDir::Files);

    //Make files other than Java unclickable
    viewModel->setNameFilters(QStringList() << "*.java");

    ui->fileListView->setModel(viewModel);
    ui->fileListView->setRootIndex(viewModel->setRootPath(userInputFilePath));

}




//========================================
//Show files in the folder highlighted in QTreeView
/**
 * @brief MasterGUI::on_fileTreeView_clicked Shows user a List View
 * of the files under a user-specified path using QListView when a
 * folder is clicked on the left most panel
 * @param index inherits the QModelIndex functionality
 * @return nothing
 */
void MasterGUI::on_fileTreeView_clicked(const QModelIndex &index)
{
    QString tempPath = viewModel->fileInfo(index).absoluteFilePath();
    ui->fileListView->setRootIndex(viewModel->setRootPath(tempPath));
}




//========================================
//Double click QLV to open a Java file.
/**
 * @brief MasterGUI::on_fileListView_doubleClicked Opens
 * a specified java file in the text viewer, which has syntax highlighting
 * feature inherit from QT examples.
 * @param index inherits the QModelIndex functionality
 * @return nothing
 */
void MasterGUI::on_fileListView_doubleClicked(const QModelIndex &index)
{
    QString tempPath = viewModel->fileInfo(index).absoluteFilePath();

    ////Alternate Code
    //    QFile fileOpener(tempPath);
    //    if(!fileOpener.open(QIODevice::ReadOnly))
    //    {
    //        QMessageBox::information(0,"info",fileOpener.errorString());
    //    }
    //    QTextStream in(&fileOpener);

    //    ui->javaViewer->setText(in.readAll());


    //Configuring the QTextEdit with syntax highlighting options
    //Maybe consider to let the user change the font/color?
    QFont javaViewerFont;
    javaViewerFont.setFamily("Courier");
    javaViewerFont.setFixedPitch(true);
    javaViewerFont.setPointSize(11);

    ui->javaViewer->setFont(javaViewerFont);
    ui->javaViewer->setReadOnly(1); //set textEdit to readOnly if user wants to drag

    //Opening Java file
    javaHighliter = new Highlighter(ui->javaViewer->document());

    if (!tempPath.isEmpty()) {
        QFile fileOpener(tempPath);

        QFileInfo tempFileInfo(fileOpener.fileName());
        //Assigning the currentClassName to the current file, so it can be used
        //for the database object.
        currentClassName = (tempFileInfo.fileName());
        //std::cerr << filename.toStdString() << std::endl;

        if (fileOpener.open(QFile::ReadOnly | QFile::Text))
        {
            QTextStream in(&fileOpener);
            ui->javaViewer->setText(in.readAll());
        }
    }
}



//========================================
//https://doc.qt.io/archives/4.6/uitools-textfinder-textfinder-cpp.html
/**
 * @brief MasterGUI::on_searchPushButton_clicked This method searches for a particular
 * string in a java file which the user specifies and highlights it if the file
 * contains the matching string.
 * @return nothing
 */
void MasterGUI::on_searchPushButton_clicked()
{
    QString searchString = ui->searchBoxLineEdit->text();
    QTextDocument *searchDocument = ui->javaViewer->document();

    bool searchFound = false;

    if(searchString.isEmpty()){
        QMessageBox::information(this, tr("Empty Search Field"),
                                 "The search field is empty. Please enter a word and click Find.");
    } else {

        QTextCursor searchHLCursor(searchDocument);
        QTextCursor searchCursor(searchDocument);

        searchCursor.beginEditBlock();

        QTextCharFormat plainFormat(searchHLCursor.charFormat());
        QTextCharFormat colorFormat = plainFormat;
        colorFormat.setForeground(Qt::red);

        while (!searchHLCursor.isNull() && !searchHLCursor.atEnd()) {
            searchHLCursor = searchDocument->find(searchString, searchHLCursor, QTextDocument::FindWholeWords);

            if (!searchHLCursor.isNull()) {
                searchFound = true;
                searchHLCursor.movePosition(QTextCursor::WordRight,
                                            QTextCursor::KeepAnchor);
                searchHLCursor.mergeCharFormat(colorFormat);
            }
        }

        searchCursor.endEditBlock();
        searchIsFirstTime = false;

        if (searchFound == false) {
            QMessageBox::information(this, tr("Word Not Found"),
                                     "Search something else.");
        }

    }
}


//========================================
//Used to develop the select/deselect function
/**
 * @brief MasterGUI::on_javaViewer_selectionChanged Lets the user select
 * a portion of text in the viewer and copy it to the global clipboard.
 * @return nothing
 */
void MasterGUI::on_javaViewer_selectionChanged()
{
    ui->javaViewer->copy(); //put selected text to clipboard
}




//========================================
/**
 * @brief MasterGUI::on_showPathPushBotton_clicked This method shows the path of
 * user inputed directory
 * @return nothing
 */
void MasterGUI::on_showPathPushBotton_clicked()
{
    QString tempPathString = userInputFilePath;

    if(tempPathString.isNull()){

        QMessageBox::information(this, tr("Wrong Path"),
                                 "Empty User Input File Path");
    } else {

        QMessageBox::information(this, tr("Wrong Path"),
                                 tempPathString);
    }
}


//========================================
/**
 * @brief MasterGUI::on_chooseDBLocationPB_clicked This method allows the
 * user to define a specific database location that he/she
 * wants to open/modify.
 * @return nothing
 */
void MasterGUI::on_chooseDBLocationPB_clicked()
{
    userInputDBPath =
            QInputDialog::getText(this,
                                  "Input DB Path",
                                  "Input a DB path you want to show/edit:");

    if(userInputDBPath.isNull()){
        QMessageBox::information(this, tr("No path entered/"),
                                 "Please input a DB path.");
        //Cannot use a DB within QResource -- since it is ReadOnly
        //userInputDBPath = ":/SampleDB/Customer.db";
    } else {
        userInputDBPath = (userInputDBPath);
    }

    editableDB = QSqlDatabase::addDatabase("QSQLITE");
    editableDB.setDatabaseName(userInputDBPath);

    editableDB.open();

    if(!editableDB.open()) //use to debug
    {
        std::cerr << "Could not open database" << std::endl;
        std::cerr << "Last error: " << editableDB.lastError().text().toStdString() << std::endl;
    }

    editableQtTableModel = new QSqlTableModel(this);
    //Save db using UI in run-time
    editableQtTableModel->setEditStrategy(QSqlTableModel::OnFieldChange);

    // Get a list of tables
    QStringList listOfTables = editableDB.tables(QSql::Tables);

    ui->dbTablesComboBox->addItems(listOfTables);

}

//========================================
//http://doc.qt.io/qt-5/qcombobox.html
/**
 * @brief MasterGUI::on_dbTablesComboBox_currentTextChanged This method
 * shows a list of tables within the DB opened and opens that table upon changes.
 * @param arg1 The name of the database being edited
 * @return nothing
 */
void MasterGUI::on_dbTablesComboBox_currentTextChanged(const QString &arg1)
{
    ////An alternate way to get the current item in QComboBox
    //QString cb = ui->dbTablesComboBox->currentText();
    QString cb = arg1;
    editableQtTableModel->setTable(cb);

    editableQtTableModel->select();

    ui->editableTableView->setModel(editableQtTableModel);
    //ui->editableTableView->show(); //not necessary

    //if there is an error
    qDebug() << editableQtTableModel->lastError().text();
}




//========================================
/**
 * @brief MasterGUI::on_getClassNamePB_clicked This method
 * gets the Java class file name by clicking on the
 * getClassNamePB.
 * @return nothing
 */
void MasterGUI::on_getClassNamePB_clicked()
{
    if(!dbLocation.empty()) {
        QMessageBox::information(this, tr("Empty Class Name"),
                                 "Open a Java file first.");
    } else {
        ui->classNameTextLabel->setText(currentClassName);
    }
}


//========================================
/**
 * @brief MasterGUI::on_DBLocationPB_clicked This method promotes
 * the user to enter a student's name, the class
 * of the Java file, a location to store the DB and the current lab name;
 * then creates the DB object with the information stated above.
 * @return nothing
 */
void MasterGUI::on_DBLocationPB_clicked()
{
    ifDBLocationPB_clicked = false;

    dbLocation =
            (QInputDialog::getText(this,
                                   "Define DB Path",
                                   "Leave blank if not needed:")).toStdString();

    studentName = (ui->studentNameInputLineEdit->text()).toStdString();
    currentClassName = (ui->classNameTextLabel->text());
    currentLabName = (ui->labNameLineEdit->text()).toStdString();

    if(!dbLocation.empty()){
        dbLocation = dbLocation;
    } else {
        dbLocation = ".";
    }

    //std::cerr <<  studentName << std::endl;

    if(studentName.empty()) {

        QMessageBox::information(this, tr("Empty Student Name"),
                                 "The Student Name field is empty. Please specify a student name.");

    } else if(currentClassName.toStdString().empty()) {

        QMessageBox::information(this, tr("Current Class Name"),
                                 "The Current Class field is empty. Please use 'Get Class Name' button to get the current class name.");

    } else if(currentLabName.empty()) {

        QMessageBox::information(this, tr("Current Lab Name"),
                                 "The Current Lab field is empty. Please specify a current lab name.");
    } else {

        QMessageBox::information(this, tr("Default DB location"),
                                 "With no specific DB location specified, default location will be used to create DB object.");

        dbTOPLEVELObj = new DBTopLevel(studentName, currentClassName.toStdString(),
                                       currentLabName, dbLocation);

        ifDBLocationPB_clicked = true;
    }

}






//========================================
//http://doc.qt.io/qt-5/qstring.html#toInt
/**
 * @brief MasterGUI::on_assignGradePB_clicked This method assigns
 * a grade to the already created DB object, if there's no DB object yet,
 * it will warn the user.
 * @return nothing
 */
void MasterGUI::on_assignGradePB_clicked()
{
    if(ifDBLocationPB_clicked){

        bool ok;
        int tempGrade = (ui->studentGradeLineEdit->text()).toInt(&ok);
        if(ok) {
            qDebug() << tempGrade;
            tempGrade = tempGrade;
        } else {
            qDebug() << "failed to read the string";
        }

        dbTOPLEVELObj->saveStudentGrade(tempGrade);

    } else {

        QMessageBox::information(this, tr("No DB Object"),
                                 "Create a DB object and then try to add score to it.");
    }
}



//========================================
/**
 * @brief MasterGUI::on_addCommentPB_clicked This method assigns
 * a grade to the already created DB object, if there's no DB object yet,
 * it will warn the user.
 * @return nothing
 */
void MasterGUI::on_addCommentPB_clicked()
{
    if(ifDBLocationPB_clicked){

        std::string studentCode = (ui->studentCodeLineEdit->text()).toStdString();
        std::string teacherComment = (ui->teacherCommentLineEdit->text()).toStdString();
        bool saveOrNot = ui->saveForLatercheckBox->isChecked();
        //std::cerr <<  saveOrNot << std::endl;

        if(studentCode.empty()){

            QMessageBox::information(this, tr("No code"),
                                     "You need to refer to some code first, note that text in "
                                     "java viewer is copied whenever the code is highlighted.");
        } else if (teacherComment.empty()){

            QMessageBox::information(this, tr("No comment"),
                                     "You need to add some comment also in order to save the comment.");
        } else {

            dbTOPLEVELObj->addComment(studentCode,teacherComment,saveOrNot);

        }

    } else {

        QMessageBox::information(this, tr("No DB Object"),
                                 "Create a DB object and then try to add comment to it.");
    }
}




//========================================
/**
 * @brief MasterGUI::toQStringVector This method converts a
 * variable of type Qvector<std::string> into a variable of
 * type QVector<QString>
 * @param aVector The variable to be converted
 * @return The converted data variable of type QVector<Qstring>
 */
QVector<QString> MasterGUI::toQStringVector(QVector<std::string> aVector)
{
    QVector<QString> vector;
    for (auto item : aVector)
    {
        QString value = QString::fromStdString(item);
        vector << value;
    }
    return vector;
}



//========================================
/**
 * @brief MasterGUI::on_findSavedCommentsPB_clicked This method
 * finds a comment saved to the persistant comments database
 * by clicking the corresponsing pushbutton
 * @return nothing
 */
void MasterGUI::on_findSavedCommentsPB_clicked()
{
    if(ifDBLocationPB_clicked) {


        std::vector<std::string> searchCombined =
                dbTOPLEVELObj->getSimiliarSavedComments(ui->searchSavedCommentsLineEdit->text().toStdString());

        QVector<std::string> searchCombinedQVEC = QVector<std::string>::fromStdVector(searchCombined);
        QVector<QString> finalQVEC = MasterGUI::toQStringVector(searchCombinedQVEC);

        QStringList tempList;
        foreach (QString str, finalQVEC) {
            tempList << str;
        }

        //if(tempList.isEmpty() == false){
        QString tempQString = tempList.join("");
        tempQString = tempList.join(",");
        //} else {
        //std::cerr <<  "QStringList is empty" << std::endl;
        //}

        if(tempQString.trimmed().isEmpty()){
            ui->showSearchSavedCommentsTextEdit->setText("No matches.");
        } else {
            //ui->showSearchSavedCommentsTextEdit->setText(tempQString);
        }
    } else {

        QMessageBox::information(this, tr("No DB Object"),
                                 "Create a DB object and then try to add comment to it.");
    }


    ////////////////==================================================

    //    std::string toBeSearched = ui->searchSavedCommentsLineEdit->text().toStdString();

    //    std::vector<std::string> comments = dbTOPLEVELObj->getSimiliarSavedComments(toBeSearched);

    //    for (int y = 0; y < comments.size(); y++) {

    //        //std::cerr << QString::fromStdString(comments.at(y)) << endl;
    //        std::cerr << (comments.at(y)) << endl;

    //    }

    ////////////////==================================================



    //    if (tempList.isEmpty())
    //      ui->showSearchSavedCommentsTextEdit->append(( tr("nothing are different") ));
    //    else {
    //      while ( !tempList.isEmpty() )
    //        ui->showSearchSavedCommentsTextEdit->append(tempList.takeFirst() );
    //    }

    //    QString tempQStr;
    //    for (int i = 0; i < searchCombinedQVEC.size(); ++i)
    //    {
    //        if (i > 0)
    //            tempQStr += " ";

    //        //std::cerr <<  searchCombinedQVEC[i] << std::endl;
    //        tempQStr += QString::fromStdString(searchCombinedQVEC[i]);
    //    }

    //ui->showSearchSavedCommentsTextEdit->setText("tempQStr");
}




//========================================
/**
 * @brief MasterGUI::on_outFileGeneratePB_clicked This method
 * generates an output file for a student by
 * clicking the Generate Outfile pushbutton
 * @return nothing
 */
void MasterGUI::on_outFileGeneratePB_clicked()
{

    if(ifDBLocationPB_clicked){

        QString tempFileOutName = "";
        tempFileOutName.append(QString::fromStdString(dbTOPLEVELObj->returnStudent()));
        tempFileOutName.append("-");
        tempFileOutName.append(QString::fromStdString(dbTOPLEVELObj->returnLabName()));
        tempFileOutName.append(".txt");

        QString fileName = tempFileOutName;

        QFile file(fileName);

        // Trying to open in WriteOnly and Text mode
        if(!file.open(QFile::WriteOnly | QFile::Text))
        {
            qDebug() << " Could not open file for writing";
            return;
        }

        QTextStream out(&file);

        out << "Student:" << " ";
        out << QString::fromStdString(dbTOPLEVELObj->returnStudent()) << endl;
        out << "-----------------------------------------" << "\n";


        out << "Class Name:" << " ";
        out << QString::fromStdString(dbTOPLEVELObj->returnCurrentClass()) << endl;
        out << "-----------------------------------------" << "\n";


        out << "Lab Name:" << " ";
        out << QString::fromStdString(dbTOPLEVELObj->returnLabName()) << endl;
        out << "-----------------------------------------" << "\n";


        out << "All Comments:" << "\n";

        std::vector<std::vector<std::string>> commini = dbTOPLEVELObj->getAllComments();
        std::vector<std::string> comments;

        for (unsigned int x = 0; x < commini.size(); x++) {
            comments = commini.at(x);
            for (unsigned int y = 0; y < comments.size(); y++) {
                out << QString::fromStdString(comments.at(y)) << "\n" << "\n";
            }
            out << "-----------------------------------------" << "\n";
        }

        file.flush();
        file.close();

    } else {

        QMessageBox::information(this, tr("No DB Object"),
                                 "Create a DB object and then try to add comment to it.");
    }
}


