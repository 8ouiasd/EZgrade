/**
* @author Xingyuan Guo
* @brief Central Main class that calls UI, does interface with DB, basically
* everything.
*/

#include "mastergui.h"
#include <QApplication>
#include <QSplashScreen>
#include <QTimer>
#include <QFile>


int main(int argc, char *argv[])
{
    QApplication EZGrade(argc, argv);

    QSplashScreen *splScreen = new QSplashScreen;
    splScreen->setPixmap(QPixmap(":/images/EZGrade.png")); //splash screen picture directory
    splScreen->show();

    MasterGUI w;

    w.setWindowTitle("EZGrade");

    QTimer::singleShot(2000,splScreen,SLOT(close()));
    QTimer::singleShot(1000,&w,SLOT(show()));

    delete splScreen; //Free up the QSC object memory

    return EZGrade.exec();
}
