#include <linenumberarea.h>

/**
 * @brief LineNumberArea::LineNumberArea This method takes the user's
 * input from the GUI referencing the line numbers of a particular set of code
 * @param editor inherits the QTextEdit functions
 * @return nothing
 */
LineNumberArea::LineNumberArea(QTextEdit *editor) : QWidget(editor) {
    codeEditor = editor;
}

/**
 * @brief LineNumberArea::sizeHint This method provides the user and program with
 * the size of the user highlighted area
 * @return nothing
 */
QSize LineNumberArea::sizeHint() const {
    return QSize(((QTextEditHighlighter *)codeEditor)->lineNumberAreaWidth(), 0);
}

/**
 * @brief LineNumberArea::paintEvent This method highlights the text which the user
 * is intending to isolate
 * @param event inherits the QPaintEvent functions
 * @return nothing
 */
void LineNumberArea::paintEvent(QPaintEvent *event) {
    ((QTextEditHighlighter *)codeEditor)->lineNumberAreaPaintEvent(event);
}
