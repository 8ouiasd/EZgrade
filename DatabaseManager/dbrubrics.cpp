#include "dbrubrics.h"

/**
 * @brief DBRubrics::DBRubrics Default constructor
 * @return nothing
 */
DBRubrics::DBRubrics()
{
}

/**
 * @brief DBRubrics::DBRubrics This custom constructor adds the rubric functionality
 * to the database based upon the filename reference
 * @param db database pointer
 * @param fileName the filename reference
 * @return nothing
 */
DBRubrics::DBRubrics(DBTool *db, std::string fileName) : DBTable(db, fileName)
{
    store_add_row_sql();
    store_create_sql();

    // must build table sepparately so new
    // sql can be properly registered
    build_table();
}

/**
 * @brief DBRubrics::~DBRubrics This destructor currently
 * holds no functionality
 * @return nothing
 */
DBRubrics::~DBRubrics(){}

/**
 * @brief DBRubrics::addRubric This method executes the adding of a rubric
 * to the database based upon multiple parameters
 * @param R5 Grading characteristic 1
 * @param R4 Grading characteristic 2
 * @param R3 Grading characteristic 3
 * @param R2 Grading characteristic 4
 * @param R1 Grading characteristic 5
 * @param tags NEED DESCRIPTION
 * @return retCode int returns the database return code for the new rubric
 */
int DBRubrics::addRubric(std::string R5, std::string R4,
                         std::string R3,  std::string R2,
                         std::string R1,  std::string tags)
{
    int   retCode = 0;
    char *zErrMsg = 0;

    sql_add_row = "INSERT INTO ";
    sql_add_row += table_name;
    sql_add_row += " VALUES ( ";

    sql_add_row += "\"";
    sql_add_row += R5;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += R4;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += R3;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += R2;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += R1;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += tags;
    sql_add_row += "\");";

    retCode = sqlite3_exec(curr_db->db_ref(), sql_add_row.c_str(), cb_add_rubric, this, &zErrMsg);

    if(retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
 * @brief cb_add_rubric This method holds no functionality
 * @param data
 * @param argc
 * @param argv
 * @param azColName
 * @return 0 int
 */
int cb_add_rubric(void *data, int argc, char **argv, char **azColName)
{
    return 0;
}

/**
 * @brief DBRubrics::select_all This method selects all the elements of
 * previously specified table
 * @return nothing
 */
bool DBRubrics::select_all(){
    int   retCode = 0;
    char *zErrMsg = 0;

    rubrics = new std::vector<std::vector<std::string>>;
    sql_select  = "SELECT * FROM ";
    sql_select += table_name;
    sql_select += ";";

    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_select.c_str(),
                           cb_select_rubric,
                           this,
                           &zErrMsg          );

    if( retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }

}

/**
 * @brief cb_select_rubric This method selects a certain rubric from
 * our database to use in future methods
 * @param data An object of the data contained within the rubric
 * @param argc Determines whether the referenced data is valid
 * @param argv This variable helps us keep track of the
 * size of the referenced data
 * @param azColName No function
 * @return int 0
 */
int cb_select_rubric(void *data, int argc, char **argv, char **azColName)
{
    std::cerr << "\n";
    std::cerr << "cb_select_rubric beign called \n";

    if(argc < 1) {
        std::cout << "No data presented to callback "
                  << "argc = " << argc
                  << std::endl;
    }


    DBRubrics *obj = (DBRubrics *) data;

    std::vector<std::string> temp;
    for(int i = 0; i < argc; i++)
    {
        temp.push_back(std::string(argv[i]));
    }
    obj->rubrics->push_back(temp);
    return 0;
}

/**
 * @brief DBRubrics::store_add_row_sql This method stores
 * a set of data in an database table row
 * @return nothing
 */
void DBRubrics::store_add_row_sql(){
    sql_template =  "SELECT name ";
    sql_template += "FROM   sqlite_master ";
    sql_template += "WHERE";
    sql_template += "    type = \"table\"";
    sql_template += ";";
}

/**
 * @brief DBRubrics::store_create_sql This method creates the
 * table template to be filled in by the add function above
 * @return nothing
 */
void DBRubrics::store_create_sql()
{
    sql_create = "CREATE TABLE ";
    sql_create += table_name;
    sql_create += " ( ";
    sql_create += " R5 STRING NOT NULL, ";
    sql_create += " R4 STRING NOT NULL, ";
    sql_create += " R3 STRING NOT NULL, ";
    sql_create += " R2 STRING NOT NULL, ";
    sql_create += " R1 STRING NOT NULL, ";
    sql_create += " tags STRING  ";
    sql_create += " );";
}
