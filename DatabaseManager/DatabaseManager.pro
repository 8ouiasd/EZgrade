TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp\
    dbtable.cpp \
    # dbtableex.cpp \
    dbtool.cpp \
    dbcomments.cpp \
    dbrubrics.cpp \
    dbtoplevel.cpp \
    dbsavedcomments.cpp \
    searchtable.cpp \
    dbstudentgrades.cpp

HEADERS += \
    dbtable.h \
    # dbtableex.h \
    dbtool.h \
    dbcomments.h \
    dbrubrics.h \
    dbtoplevel.h \
    dbsavedcomments.h \
    searchtable.h \
    dbstudentgrades.h

LIBS += -l sqlite3
