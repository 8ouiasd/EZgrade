#include "dbsavedcomments.h"

/**
 * @brief DBSavedComments::DBSavedComments This default constructor sets out
 * pointer SearchTable object to a new searchTable object
 * @return nothing
 */
DBSavedComments::DBSavedComments()
{
    searchTable = new SearchTable;
}

/**
 * @brief DBSavedComments::DBSavedComments This constructor builds an
 * initial table based on the filename and database tool
 * @param db The database reference of the new table
 * @param fileName The filename reference of the table
 * @return nothing
 */
DBSavedComments::DBSavedComments(DBTool *db, std::string fileName) :
    DBTable(db,fileName)
{
    //tN = fileName;
    store_add_row_sql();
    store_create_sql();

    // must build table sepparately so new
    // sql can be properly registered
    build_table();
}

/**
 * @brief DBSavedComments::~DBSavedComments This destructor deallocates
 * our searchTable and comments objects
 */
DBSavedComments::~DBSavedComments()
{
    delete searchTable;
    delete comments;
}

/**
* @brief DBSavedComments::store_add_row_sql Setup for rows, inherited from pfaffman's code
* Inherited from Professor Pfaffman's code, this method sets up the row portion of the
* comments table
* @return nothing
*/
void DBSavedComments::store_add_row_sql() {

    sql_template =  "SELECT name ";
    sql_template += "FROM   sqlite_master ";
    sql_template += "WHERE";
    sql_template += "    type = \"table\"";
    sql_template += ";";

}

/**
* @brief DBSavedComments::store_create_sql Creates the initial sql table and names
* certain features
* @return nothing
*/
void DBSavedComments::store_create_sql()
{
    sql_create = "CREATE TABLE ";
    sql_create += table_name;
    sql_create += " ( ";
    sql_create += " Comment STRING NOT NULL";
    sql_create += " );";
}

/**
 * @brief DBSavedComments::addComment This method takes the input
 * comment parameter and adds the comment to the previously
 * specified table name
 * @param comment The comment being added the database
 * @return int retCode Integer representation of the
 * database reference
 */
int DBSavedComments::addComment(std::string comment)
{
    int   retCode = 0;
    char *zErrMsg = 0;

    sql_add_row = "INSERT INTO ";
    sql_add_row += table_name;
    sql_add_row += " VALUES ";
    sql_add_row += " ( ";
    sql_add_row += "\"";
    sql_add_row += comment;
    sql_add_row += "\" ); ";


    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_add_row.c_str(),
                           cb_add_saved_comment,
                           this,
                           &zErrMsg);

    if(retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
 * @brief DBSavedComments::searchComments This method takes an inputed string search term
 * and searches the database for matches
 * @param searchTerms The input string which is being searched for
 * @return std::vector<std::string> relevanceSort This return value a
 * vector of strings with all of the term matches
 */
std::vector<std::string> DBSavedComments::searchComments(std::string searchTerms)
{
    std::cout << "HERE";
    delete searchTable;
    searchTable = new SearchTable;
    select_all();
    for(unsigned x = 0; x < comments->size(); x++)
    {
        searchTable->addComment(comments->at(x));
    }

    std::vector<std::string> relevanceSort;

    std::vector<int> revVal = searchTable->getRelevence(searchTerms);

    std::vector<std::vector<int>> values;
    //Make a vector that contains the relevance, and position
    for(int x = 0; x < revVal.size(); x++)
    {

        values.push_back(std::vector<int>{revVal[x], x});
    }

    for(int i = 1; i < values.size(); i ++)
    {
        std::vector<int> j = values[i];
        int relevanceValue = values[i][0];
        int index = i-1;
        while((index >= 0) && relevanceValue > values[index][0])
        {
            values[index + 1] = values[index];
            index = index - 1;
        }
        values[index + 1] = j;
    }


    for(int x = 0; x < values.size(); x++)
    {
        std::cout << values[x][0] << "  " << values[x][1] <<  "\n";
        if(values[x][0] != 0)
        {
            relevanceSort.push_back(comments->at(values[x][1]));
        }
    }
    return relevanceSort;
}

/**
 * @brief cb_add_saved_comment This method adds previously used teacher
 * comments to the database for future use
 * @param data No function
 * @param argc No function
 * @param argv No function
 * @param azColName No function
 * @return int 0
 */
int cb_add_saved_comment(void  *data,
                   int    argc,
                   char **argv,
                   char **azColName)
{
    std::cerr << "\n";
    std::cerr << "cb_add_saved_comment being called\n" << std::endl;
    return 0;
}

/**
 * @brief DBSavedComments::select_all This method selects all of the
 * saved teacher comments
 * @return int retCode Integer representation of the size
 * of out database reference
 */
bool DBSavedComments::select_all()
{
    comments = new std::vector<std::string>;
    int   retCode = 0;
    char *zErrMsg = 0;

    sql_select  = "SELECT * FROM ";
    sql_select += table_name;
    sql_select += ";";

    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_select.c_str(),
                           cb_select_allComments,
                           this,
                           &zErrMsg          );

    if( retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }

    return retCode;
}

/**
 * @brief cb_select_allComments This method, called by the method above selects
 * all of the saved comments from the database
 * @param data The object which represents the referenced data
 * @param argc This variable determines whether the refernced
 * data is valid or not
 * @param argv Used to select the first comment of the returned reference data
 * @param azColName No function
 * @return int 0
 */
int cb_select_allComments(void  *data,
                  int    argc,
                  char **argv,
                  char **azColName)
{
    std::cerr << "cb_select_allComments being called\n";
    if(argc < 1) {
        std::cerr << "No data presented to callback "
                  << "argc = " << argc
                  << std::endl;
    }

    DBSavedComments *obj = (DBSavedComments *) data;

    std::cout << "------------------------------\n";
    std::cout << obj->get_name()
              << std::endl;

    std::string temp;
    temp = std::string(argv[0]);
    obj->comments->push_back(temp);
    return 0;
}


