#ifndef DBTOPLEVEL_H
#define DBTOPLEVEL_H

#include <iostream>
#include <vector>

#include "dbcomments.h"
#include "dbrubrics.h"
#include "dbsavedcomments.h"
#include "dbstudentgrades.h"
#include "dbtable.h"
#include "dbtool.h"

class DBTopLevel
{
public:
    DBTopLevel();
    DBTopLevel(std::string studentName, std::string currentClass, std::string  labName, std::string fileLocation);
    ~DBTopLevel();

    bool setFileLocation(std::string fileLocation);//Used to set where to find the diffrent required rubrics
    bool changeLab(std::string labName);
    bool changeStudent(std::string studentName);
    bool changeCurrentClass(std::string currentClass);

    std::string returnStudent();
    std::string returnCurrentClass();
    std::string returnLabName();
    std::string returnFileLocation();

    bool deleteComment(int index);

    /**
     * @brief addComment Add a comment to student code
     * @param className The name of the class that will be added to
     * @param lineStart The line start number
     * @param studentCode Code from the sutdent
     * @param teacherComment Comment made by the teacher
     * @param saveForLater Save the comment for resuse later. This will just save the words of the comment itself, and no other information
     * @return
     */
    bool addComment(std::string studentCode, std::string teacherComment, bool saveForLater);
    bool addRubric (std::string R5,  std::string R4,
                    std::string R3,  std::string R2,
                    std::string R1, bool saveForLater);
    bool saveStudentGrade(int score);

    std::vector<std::vector<std::string>> getAllComments();
    std::vector<std::vector<std::string>> getCurrentClassComments();
    std::vector<std::vector<std::string>> getAllLabRubrics();
    std::vector<std::string> getAllSavedComments();
    std::vector<std::string> getSimiliarSavedComments(std::string searchComment);



private:
    std::string studentName;
    std::string currentClass;
    std::string fileLocation;
    std::string labName;

    DBTool *toolCurrentLab;
    DBTool *toolPersistantInfo;

    DBComments *studentCommentsTable;
    DBSavedComments *savedCommentsTable;
    DBRubrics  *rubricsTable; //Current rubrics used by the lab

    DBStudentGrades *gradeTables;
    DBRubrics *pastRubricsTable;
};


#endif // DBTOPLEVEL_H
