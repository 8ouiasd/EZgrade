#include "searchtable.h"

/**
 * @brief SearchTable::SearchTable This default constructor
 * creates a new relevanceAmmount object
 * @return nothing
 */
SearchTable::SearchTable()
{
    relevanceAmmount = new std::vector<int>;
}

/**
 * @brief SearchTable::~SearchTable This destructor
 * deallocates our relevanceAmmount pointer
 * @return nothing
 */
SearchTable::~SearchTable()
{
    //delete relevanceAmmount;
}

/**
 * @brief SearchTable::stringSpliter This method searches
 * through sets of long strings and separates certain
 * sections into isolated strings
 * @param splitWord The string being split up
 * @return std::vector<std::string> subString returns an
 * isolated string which was contained in the splitWord string
 */
std::vector<std::string> SearchTable::stringSpliter(std::string splitWord)
{
    //Turn the string into all lower cases
    for(unsigned i = 0; i < splitWord.size(); i++)
    {
        splitWord[i] = std::tolower(splitWord[i]);
    }

    //split into substring by spaces
    std::vector<std::string> subString;
    int upper = 0;
    while(upper != splitWord.size())
    {
        //find the first location of a space
        if((upper = splitWord.find(" ")) == -1)
        {
            upper = splitWord.size();
        }
        //get a string from the begging to the location of the " "
        std::string tempWord = splitWord.substr(0, upper);
        for(unsigned i =0; i < removeLetters.size(); i++)
        {
            tempWord.erase(std::remove(tempWord.begin(), tempWord.end(), removeLetters[i]), tempWord.end());
        }
        //Remove extra words i dont want
        bool validWord = true;
        for(unsigned i = 0; i < removeWords.size(); i++)
        {
            if(tempWord == removeWords[i])
            {
                validWord = false;
            }
        }
        if(validWord)
        {
            subString.push_back(tempWord);
        }
        //Remove the string from the start of the sentence
        splitWord.erase(0, upper+1);
    }
    return subString;
}

/**
 * @brief SearchTable::addComment This method adds a
 * comment in th form of a string to the current table
 * @param comment The comment being added
 * @return bool true
 */
bool SearchTable::addComment(std::string comment)
{
    std::string tempComment = comment;

    //Add the split and modified comment to the table
    table.push_back(stringSpliter(tempComment));

    return true;
}

/**
 * @brief SearchTable::getRelevence This method returns the
 * number of times the searched string occurs in the current
 * table
 * @param search The string being searched for
 * @return std::vector<int> relevanceAmmount The number of times
 * the searched for string is found in the searched table
 */
std::vector<int> SearchTable::getRelevence(std::string search)
{
    std::vector<std::string> searchTerms = stringSpliter(search);
    relevanceAmmount = new std::vector<int>;

    for(unsigned i = 0; i < table.size(); i ++)
    {
        int relevance = 0;
        for(unsigned k = 0; k < table[i].size(); k ++)
        {
            for(unsigned x = 0; x < searchTerms.size(); x ++)
            {
                if(searchTerms[x] == table[i][k])
                {
                    //std::cout << searchTerms[x] << "  " << table[i][k] << "\n";
                    relevance += 1;
                }
            }
        }
        relevanceAmmount->push_back(relevance);
    }
    return *relevanceAmmount;
}
