#ifndef DBSTUDENTGRADES_H
#define DBSTUDENTGRADES_H

#include <vector>
#include <iostream>
#include <string>
#include <sqlite3.h>

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "dbtool.h"
#include "dbtable.h"

#include <cstring>

class DBStudentGrades : public DBTable
{
public:
    DBStudentGrades();
    DBStudentGrades(DBTool *db, std::string fileName);
    ~DBStudentGrades();

    void store_create_sql();
    void store_add_row_sql();

    int addGrading(std::string labName, int labGrade);
    bool select_all();

    std::vector<std::vector<std::string>> *grades;
};
int cb_add_studentGrade(void  *data,
                        int    argc,
                        char **argv,
                        char **azColName);

int cb_select_all_grades(void  *data,
                         int    argc,
                         char **argv,
                         char **azColName);
#endif // DBSTUDENTGRADES_H
