#include "dbtoplevel.h"

/**
 * @brief DBTopLevel::DBTopLevel This default constructor is the
 * top side database manager
 * @return nothing
 */
DBTopLevel::DBTopLevel()
{
    studentName = "DoeJ";
    currentClass = "main.java";
    labName = "ExLab";
    fileLocation = ".";

    toolCurrentLab = new DBTool(fileLocation, labName);
    toolPersistantInfo = new DBTool(fileLocation, "StudentGrades");

    changeLab(this->labName);
}

/**
 * @brief DBTopLevel::DBTopLevel This constructor is the top side
 * database manager
 * @param studentName The student name of the first person who used
 * @param currentClass The first class used. Should probably be
 * main.java
 * @param labName The lab name
 * @param fileLocation Where the user is storing its information
 * @return nothing
 */
DBTopLevel::DBTopLevel(std::string studentName, std::string currentClass,
                       std::string  labName, std::string fileLocation)
{
    this->studentName = studentName;
    this->currentClass = currentClass;
    this->labName = labName;
    this->fileLocation = fileLocation;

    toolCurrentLab = new DBTool(this->fileLocation, this->labName);
    toolPersistantInfo = new DBTool(this->fileLocation, "StudentGrades");

    changeLab(this->labName);
}

/**
 * @brief DBTopLevel::~DBTopLevel This destructor deletes all the pointers
 */
DBTopLevel::~DBTopLevel()
{
    delete toolCurrentLab;
    delete toolPersistantInfo;
    delete studentCommentsTable;
    //delete savedCommentsTable;
    delete rubricsTable;
    delete gradeTables;
    delete pastRubricsTable;
}

/**
 * @brief DBTopLevel::changeStudent This method changes the current student
 * @param studentName Name of the student to change to
 * @return
 */
bool DBTopLevel::changeStudent(std::string studentName)
{
    this->studentName = studentName;
    gradeTables = new DBStudentGrades(toolPersistantInfo, this->studentName);
    studentCommentsTable = new DBComments(toolCurrentLab, this->studentName + "Comments");
    return true;
}

/**
 * @brief DBTopLevel::changeCurrentClass This method changes
 * the current class being referenced
 * @param currentClass The new class that will be looked at
 * @return bool true
 */
bool DBTopLevel::changeCurrentClass(std::string currentClass)
{
    this->currentClass = currentClass;
    return true;
}

/**
 * @brief DBTopLevel::changeLab This method changes the lab
 * that is being edited
 * @param labName The name of the lab to be referenced
 * @return bool true
 */
bool DBTopLevel::changeLab(std::string labName)
{
    this->labName = labName;
    toolCurrentLab = new DBTool(fileLocation, labName);
    savedCommentsTable = new DBSavedComments(toolCurrentLab, "savedComments");
    rubricsTable = new DBRubrics(toolCurrentLab, "labRubrics");
    changeStudent(this->studentName);

    return true;
}

/**
 * @brief DBTopLevel::returnStudent This method returns
 * the name of the student being referenced
 * @return std::string studentName Name of the student
 */
std::string DBTopLevel::returnStudent()
{
    return studentName;
}

/**
 * @brief DBTopLevel::returnLabName This method returns
 * the name of the lab being referenced
 * @return std::string labName the name of the lab
 */
std::string DBTopLevel::returnLabName()
{
    return labName;
}

/**
 * @brief DBTopLevel::returnFileLocation This method returns
 * the name of the curretnt file location
 * @return std::string fileLocation the location
 * of the current file
 */
std::string DBTopLevel::returnFileLocation()
{
    return fileLocation;
}

/**
 * @brief DBTopLevel::addComment This method adds a
 * comment for the current student
 * @param studentCode The code from the student that
 * will be commented on
 * @param teacherComment The comment made by the student
 * @param saveForLater Choose to save the comment to
 * the saved comments file
 * @return bool If the operation was successful
 */
bool DBTopLevel::addComment(std::string studentCode,
                            std::string teacherComment, bool saveForLater)
{
    int temp = studentCommentsTable->addComment(currentClass, teacherComment, studentCode);
    int temp2 = 0;
    if(saveForLater)
    {
        temp2 = savedCommentsTable->addComment(teacherComment);
    }
    return (temp == temp2 == 0);
}

/**
 * @brief DBTopLevel::addRubric Add a rubric to the current lab
 * @param R5 5 point requirment, Grading characteristic 5
 * @param R4 4 point requirment, Grading characteristic 4
 * @param R3 3 point requirment, Grading characteristic 3
 * @param R2 2 point requirment, Grading characteristic 2
 * @param R1 1 point requirment, Grading characteristic 1
 * @return bool If operation was successful
 */
bool DBTopLevel::addRubric(std::string R5, std::string R4, std::string R3, std::string R2, std::string R1, bool saveForLater)
{
    int temp = rubricsTable->addRubric(R5,R4,R3,R2,R1,"");
    if(saveForLater)
    {
        pastRubricsTable->addRubric(R5,R4,R3,R2,R1,"");
    }
    return (temp == 0);
}

/**
 * @brief DBTopLevel::saveStudentGrade This method saves the
 * student grade into grading table
 * @param score The grade that the student got on a lab
 * @return bool true
 */
bool DBTopLevel::saveStudentGrade(int score)
{
    gradeTables->addGrading(labName, score);
    return true;
}

/**
 * @brief DBTopLevel::deleteComment This method deletes a comment based on index,
 * this index should be gotten from returned comments
 * @param index location of the comment in the comment table
 * @return bool If operation was successful
 */
bool DBTopLevel::deleteComment(int index)
{
    int temp = studentCommentsTable->deleteComment(index);
    return (temp == 0);
}

/**
 * @brief DBTopLevel::getAllComments This method returns all the comments
 * from the current lab and student
 * @return std::vector<std::vector<std::string>> *temp pointer to the
 * comments within our comments table
 */
std::vector<std::vector<std::string>> DBTopLevel::getAllComments()
{
    studentCommentsTable->select_all();
    std::vector<std::vector<std::string>> *temp = studentCommentsTable->comments;
    return *temp;
}

/**
 * @brief DBTopLevel::getCurrentClassComments This method returns all the
 * comments from the current lab, student, and class
 * @return std::vector<std::vector<std::string>> *temp pointer to the
 * comments within our comments table
 */
std::vector<std::vector<std::string>> DBTopLevel::getCurrentClassComments()
{
    studentCommentsTable->getComments(currentClass);
    std::vector<std::vector<std::string>> *temp = studentCommentsTable->comments;
    return *temp;
}

/**
 * @brief DBTopLevel::getAllSavedComments This method returns all the
 * comments that the teacher has saved
 * @return std::vector<std::string> *temp pointer to the
 * comments within our saved comments table
 */
std::vector<std::string> DBTopLevel::getAllSavedComments()
{
    savedCommentsTable->select_all();
    std::vector<std::string> *temp = savedCommentsTable->comments;
    return *temp;
}

/**
 * @brief DBTopLevel::getSimiliarSavedComments This method returns the
 * most similiar terms from the the saved comments
 * in order, doesnt show any that have a length of 0
 * @param searchComment The comment being searched for
 * @return std::vector<std::string> temp returns the selected
 * saved comment
 */
std::vector<std::string> DBTopLevel::getSimiliarSavedComments(std::string searchComment)
{
    std::vector<std::string> temp = savedCommentsTable->searchComments(searchComment);
    return temp;
}

/**
 * @brief DBTopLevel::getAllLabRubrics This method returns
 * all the current lab's rubrics.
 * @return std::vector<std::vector<std::string>> *temp returns
 * all of the rubrics contained in the rubric table
 */
std::vector<std::vector<std::string>> DBTopLevel::getAllLabRubrics()
{
    rubricsTable->select_all();
    std::vector<std::vector<std::string>> *temp = rubricsTable->rubrics;
    return *temp;
}

/**
 * @brief DBTopLevel::returnCurrentClass This method returns
 * the name of the current class as a string
 * @return std::string currentClass
 */
std::string DBTopLevel::returnCurrentClass()
{
    return currentClass;
}


