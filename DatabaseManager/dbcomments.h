#ifndef DBCOMMENTS_H
#define DBCOMMENTS_H

#include <vector>
#include <iostream>
#include <string>
#include <sqlite3.h>

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "dbtool.h"
#include "dbtable.h"

#include <cstring>


class DBComments : public DBTable
{
public:
    DBComments();
    DBComments(DBTool *db, std::string fileName);
    ~DBComments();

    //Create the sql data file
    void store_create_sql();
    void store_add_row_sql();
    //Add a comment to the sql file
    int addComment(std::string className, std::string teacherComment, std::string studentCode);
    //Modify a comment
    int ammendComment(std::string className,int lineStart, std::string teacherComment);
    //delete a comment by index
    int deleteComment(int index);
    //return comments
    int getComments(std::string className);
    //Returns the number of rows
    int rowCount();
    //std::string tN;//Table Name
    //DBTool db;
    int select_all();

    std::vector<std::vector<std::string>> *comments;
    int numComments;
};

//Callback for addComment
int cb_add_comment(void  *data,
                   int    argc,
                   char **argv,
                   char **azColName);
//Callback for editing a comment
int cb_modify_comment(void  *data,
                      int    argc,
                      char **argv,
                      char **azColName);
//Prints out all the data
int cb_select_all(void  *data,
                  int    argc,
                  char **argv,
                  char **azColName);

int cb_select_classC(void  *data,
                     int    argc,
                     char **argv,
                     char **azColName);
int cb_delete_comment(void  *data,
                      int    argc,
                      char **argv,
                      char **azColName);

#endif // DBCOMMENTS_H
