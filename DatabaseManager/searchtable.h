#ifndef SEARCHTABLE_H
#define SEARCHTABLE_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

class SearchTable
{
public:
    SearchTable();
    ~SearchTable();

    std::vector<std::vector<std::string>> table;
    bool addComment(std::string comment);
    std::vector<std::string> stringSpliter(std::string splitWord);
    std::vector<int> getRelevence(std::string search);
    std::vector<int> *relevanceAmmount;


private:
    const std::vector<std::string> removeWords = {"a", "the", "is", "of", ""};
    const std::vector<char> removeLetters = {'!',',', '?', '!'};
};


#endif // SEARCHTABLE_H
