#include "dbstudentgrades.h"

/**
 * @brief DBStudentGrades::DBStudentGrades This constructor creates an
 * initial table in the database and initializes the 2d vector
 * of strings which will store the student grades
 * @param db The name of the database being referenced
 * @param fileName The filename associated with the database table
 * @return nothing
 */
DBStudentGrades::DBStudentGrades(DBTool *db, std::string fileName)
    :DBTable(db, fileName)
{
    grades = new std::vector<std::vector<std::string>>;

    store_add_row_sql();
    store_create_sql();

    build_table();
}

/**
 * @brief DBStudentGrades::store_add_row_sql This method initializes the
 * rows of the tables of our database
 * @return nothing
 */
void DBStudentGrades::store_add_row_sql() {

    sql_template =  "SELECT name ";
    sql_template += "FROM   sqlite_master ";
    sql_template += "WHERE";
    sql_template += "    type = \"table\"";
    sql_template += ";";

}

/**
 * @brief DBStudentGrades::~DBStudentGrades This destructor deallocates
 * the grade object thus deleting the unsaved/unstored grades
 * @return nothing
 */
DBStudentGrades::~DBStudentGrades()
{
    delete grades;
}

/**
 * @brief DBStudentGrades::store_create_sql This method is used
 * to create our database table based upon out previously
 * determined table name reference
 * @return nothing
 */
void DBStudentGrades::store_create_sql()
{
    sql_create = "CREATE TABLE ";
    sql_create += table_name;
    sql_create += " ( ";
    sql_create += " LabName STRING NOT NULL, ";
    sql_create += " labGrade int NOT NULL ";
    sql_create += " );";
}

/**
 * @brief DBStudentGrades::addGrading This method uses the lab reference to
 * add the referenced grade of a studetn into the database table
 * @param labName The name of the lab which the grade is referenced to
 * @param labGrade The grade of the student with lab reference
 * @return int retCode Integer representation of the size of
 * the data returned from the database
 */
int DBStudentGrades::addGrading(std::string labName, int labGrade)
{
    int retCode = 0;
    char *zErrMsg = 0;

    sql_add_row = "INSERT INTO ";
    sql_add_row += table_name;
    sql_add_row += " VALUES ";
    sql_add_row += " ( ";

    sql_add_row += "\"";
    sql_add_row += labName;
    sql_add_row += "\", ";

    std::string temp = std::to_string(labGrade);
    sql_add_row += "\"";
    sql_add_row += temp;
    sql_add_row += "\" ";

    sql_add_row += " ); ";


    retCode = sqlite3_exec(curr_db->db_ref(),
                            sql_add_row.c_str(),
                           cb_add_studentGrade,
                           this,
                           &zErrMsg);
    if(retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
 * @brief cb_add_studentGrade This method currently has no function
 * @param data No function
 * @param argc No function
 * @param argv No function
 * @param azColName No function
 * @return int 0
 */
int cb_add_studentGrade(void *data, int argc, char **argv, char **azColName)
{
    return 0;
}

/**
 * @brief DBStudentGrades::select_all This method selects all of the students
 * grades of a certain student
 * @return nothing
 */
bool DBStudentGrades::select_all()
{
    int   retCode = 0;
    char *zErrMsg = 0;

    grades = new std::vector<std::vector<std::string>>;
    sql_select  = "SELECT * FROM ";
    sql_select += table_name;
    //sql_select += " WHERE Class = ";
    //sql_select += "\"" + labName + "\"";
    sql_select += ";";

    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_select.c_str(),
                           cb_select_all_grades,
                           this,
                           &zErrMsg          );


    if( retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
}

/**
 * @brief cb_select_all_grades This method, called by the above function
 * searches through the returned data from the table and returns all of
 * the grades
 * @param data This object represents the data from the previously
 * referenced table
 * @param argc This variable determines whether the referenced
 * data is valid or not
 * @param argv This variable helps us keep track of the size
 * of the data being searched through
 * @param azColName This variable keeps track of the current table row
 * which contains the data we are referencing
 * @return int 0
 */
int cb_select_all_grades(void *data, int argc, char **argv, char **azColName)
{
    std::cerr << "\n";
    std::cerr << "cb_select_classC beign called \n";

    if(argc < 1) {
        std::cout << "No data presented to callback "
                  << "argc = " << argc
                  << std::endl;
    }

    int i;

    DBStudentGrades *obj = (DBStudentGrades *) data;

    std::cout << "------------------------------\n";
    std::cout << obj->get_name()
              << std::endl;

    //obj->numComments = argc;

   // int size = obj->comments->size();
    std::vector<std::string> temp;
    for(i = 0; i < argc; i++){
        temp.push_back(std::string(argv[i]));

        std::cout << azColName[i]
                     << " = "
                      <<  (argv[i] ? argv[i] : "NULL")
                       << std::endl;

    }
    obj->grades->push_back(temp);
    return 0;
}
