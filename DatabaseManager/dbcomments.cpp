#include "dbcomments.h"


/**
* @author Alex Andrews
* @brief Each comment database will cover all the comments on one
* specific lab for a specific student
* File Name Designation:
*/

/**
 * @brief DBComments::DBComments Default constructor
 * */
DBComments::DBComments(){}

/**
* @brief DBComments::DBComments A custom constructor
* which initializes the comments program
* @param db The database that will be used
* @param fileName The name of the table to be used
* @return nothing
*/
DBComments::DBComments(DBTool *db, std::string fileName) :
    DBTable(db,fileName)
{
    comments = new std::vector<std::vector<std::string>>;
    //tN = fileName;
    store_add_row_sql();
    store_create_sql();

    // must build table sepparately so new
    // sql can be properly registered
    build_table();
}

/**
* @brief DBComments::~DBComments A deconstructor, deletes our comments onject
* @return nothing
*/
DBComments::~DBComments()
{
    delete comments;
}

/**
* @brief DBComments::store_add_row_sql Setup for rows, inherited from pfaffman's code
* Inherited from Professor Pfaffman's code, this method sets up the row portion of the
* comments table
* @return nothing
*/
void DBComments::store_add_row_sql() {

    sql_template =  "SELECT name ";
    sql_template += "FROM   sqlite_master ";
    sql_template += "WHERE";
    sql_template += "    type = \"table\"";
    sql_template += ";";

}

/**
* @brief DBComments::store_create_sql Creates the initial sql table and names
* certain features
* @return nothing
*/
void DBComments::store_create_sql()
{
    sql_create = "CREATE TABLE ";
    sql_create += table_name;
    sql_create += " ( ";
    sql_create += " Class STRING NOT NULL, ";
    sql_create += " TeacherComment STRING NOT NULL, ";
    sql_create += " StudentCode STRING ";
    sql_create += " );";
}

/**
* @brief DBComments::addComment This method allows you to add a new comment about
* the students code
* @param className Name of the class being referenced in the comment,
* you don't have to include file type i.e. .java
* @param lineStart Start line of the code. Used for finding and organizing the database
* @param teacherComment The comment made by the teacher
* @param studentCode The code that is being commented on
* @return int retCode This value determines whether the new comment
* is written to the comment file
*/
int DBComments::addComment(std::string className, std::string teacherComment, std::string studentCode)
{
    int   retCode = 0;
    char *zErrMsg = 0;

    std::cout << "LOOK HERE" << std::endl;

    sql_add_row = "INSERT INTO ";
    sql_add_row += table_name;
    sql_add_row += " VALUES ";
    sql_add_row += " ( ";

    sql_add_row += "\"";
    sql_add_row += className;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += teacherComment;
    sql_add_row += "\", ";

    sql_add_row += "\"";
    sql_add_row += studentCode;
    sql_add_row += "\" ";

    sql_add_row += " );";

    std::cout << "LOOK HERE2" << std::endl;
    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_add_row.c_str(),
                           cb_add_comment,
                           this,
                           &zErrMsg);

    if(retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
* @brief DBComments::ammendComment This method allows the teacher to change
* any specific previously made comment
* @param className The class the specific comment is referenced to
* @param lineStart The start line of the code
* @param teacherComment The new comment made by the teacher
* @return int retCode This value determines whether the new edited comment
* is written to the comment file
*/
int DBComments::ammendComment(std::string className, int lineStart, std::string teacherComment)
{
    int   retCode = 0;
    char *zErrMsg = 0;

    char  tempval[128];
    sql_update  = "UPDATE " + table_name;
    sql_update += " SET TeacherComment = \"" + teacherComment + "\"";
    sql_update += " WHERE Class = \"" + className + "\"";

    sprintf (tempval , "%d", lineStart);
    sql_update += " AND LineStart = ";
    sql_update += tempval;

    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_update.c_str(),
                           cb_modify_comment,
                           this,
                           &zErrMsg);

    if(retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
 * @brief DBComments::getComments Get comments from a specific class
 * @param className The class from which you want comments
 * @return int retCode Integer representation of our database reference
 */
int DBComments::getComments(std::string className)
{
    int   retCode = 0;
    char *zErrMsg = 0;

    comments = new std::vector<std::vector<std::string>>;
    sql_select  = "SELECT * FROM ";
    sql_select += table_name;
    sql_select += " WHERE Class = ";
    sql_select += "\"" + className + "\"";
    sql_select += ";";

    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_select.c_str(),
                           cb_select_classC,
                           this,
                           &zErrMsg          );


    if( retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    //std::cout << cheese.at(0);
    return retCode;
}

/**
 * @brief cb_add_comment This method is passed as an argument to
 * the addComment method and is executed before the calling function
 * can continue
 * @param *data An object of the data/comment being added
 * @param argc No function
 * @param **argv No function
 * @param **azColName No function
 * @return int 0
 */
int cb_add_comment(void *data, int argc, char **argv, char **azColName)
{
    DBComments *obj = (DBComments *) data;
    std::cerr << "\n";
    std::cerr << "cb_add_comment being called\n" << std::endl;
    return 0;
}

/**
 * @brief cb_modify_comment This method is passed as an argument to
 * the ammendComment method and is executed before the calling function
 * can continue
 * @param *data An object of the modified data/comment being added
 * @param argc This variable determines whether the referenced comment exists
 * @param **argv No function
 * @param **azColName No function
 * @return int 0
 */
int cb_modify_comment(void *data, int argc, char **argv, char **azColName)
{
    std::cerr << "cb_modify_comment beign called \n";

    if(argc < 1) {
        std::cerr << "No data presented to callback "
                  << "argc = " << argc
                  << std::endl;
    }
    int i;

    DBComments *obj = (DBComments *) data;

    std::cout << "------------------------------\n";
    std::cout << obj->get_name()
              << std::endl;
    return 0;

}

/**
 * @brief cb_select_classC This method is passed as an argument to
 * the getComments method and is executed before the calling function
 * can continue
 * @param data This is on object of the referenced data
 * @param argc This variable determines whether the referenced data is valid
 * @param argv This variable allows us to determine the size
 * of the referenced data
 * @param azColName This value is the column number of each comment
 * being added/modified/referenced
 * @return 0 int
 */
int cb_select_classC(void *data, int argc, char **argv, char **azColName)
{
    std::cerr << "\n";
    std::cerr << "cb_select_classC beign called \n";

    if(argc < 1) {
        std::cout << "No data presented to callback "
                  << "argc = " << argc
                  << std::endl;
    }

    int i;

    DBComments *obj = (DBComments *) data;

    std::cout << "------------------------------\n";
    std::cout << obj->get_name()
              << std::endl;

    obj->numComments = argc;


    //memcpy(obj->comments, argv, sizeof(&argv));

    int size = obj->comments->size();
    std::vector<std::string> temp;
    for(i = 0; i < argc; i++){
        temp.push_back(std::string(argv[i]));

        std::cout << azColName[i]
                     << " = "
                      <<  (argv[i] ? argv[i] : "NULL")
                       << std::endl;

    }
    obj->comments->push_back(temp);
    return 0;
}

/**
 * @brief DBComments::select_all This method allows the user to select a certain
 * portion of a comment or comments
 * @return int retCode An integer value that represents data
 * from our database reference
 */
int DBComments::select_all() {
    int   retCode = 0;
    char *zErrMsg = 0;

    sql_select  = "SELECT * FROM ";
    sql_select += table_name;
    sql_select += ";";

    retCode = sqlite3_exec(curr_db->db_ref(),
                           sql_select.c_str(),
                           cb_select_all,
                           this,
                           &zErrMsg          );

    if( retCode != SQLITE_OK ){
        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
 * @brief cb_select_all This method is passed as an argument to
 * the ammendComment method and is executed before the calling function
 * can continue
 * @param *data This object is used to access the referenced data
 * @param argc Determines whether the referenced data is valid
 * @param **argv This variable lets us keep track of the
 * size of the referenced data
 * @param **azColName This value is the column number of each comment being manipulated
 * @return int 0
 */
int cb_select_all(void  *data,
                  int    argc,
                  char **argv,
                  char **azColName)
{
    std::cerr << "cb_select_all db_comments being called\n";

    if(argc < 1) {
        std::cerr << "No data presented to callback "
                  << "argc = " << argc
                  << std::endl;
    }

    int i;

    DBComments *obj = (DBComments *) data;

    std::cout << "------------------------------\n";
    std::cout << obj->get_name()
              << std::endl;


    obj->numComments = argc;

    int size = obj->comments->size();
    std::vector<std::string> temp;
    for(i = 0; i < argc; i++){
        temp.push_back(std::string(argv[i]));

        std::cout << azColName[i]
                     << " = "
                      <<  (argv[i] ? argv[i] : "NULL")
                       << std::endl;

    }
    obj->comments->push_back(temp);
    return 0;
}

/**
 * @brief DBComments::deleteComment This method selects a comment based upon a
 * given index and deletes it from the database
 * @param index The location of the comment we want to delete
 * @return int retCode Integer representation of our database reference
 */
int DBComments::deleteComment(int index)
{
    int   retCode = 0;
    char *zErrMsg = 0;

    sql_update = "DELETE FROM ";
    sql_update += table_name;
    sql_update += " WHERE ROWID = ";
    sql_update += (std::to_string(index));
    sql_update += " ;";

    retCode = sqlite3_exec(curr_db->db_ref(), sql_update.c_str(),
                           cb_delete_comment, this, &zErrMsg);
    if( retCode != SQLITE_OK ){

        std::cerr << table_name
                  << " template ::"
                  << std::endl
                  << "SQL error: "
                  << zErrMsg;

        sqlite3_free(zErrMsg);
    }
    return retCode;
}

/**
 * @brief cb_delete_comment
 * @param data
 * @param argc
 * @param argv
 * @param azColName
 * @return
 */
int cb_delete_comment(void *data, int argc, char **argv, char **azColName)
{

}
