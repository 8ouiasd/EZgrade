#ifndef DBRUBRICS_H
#define DBRUBRICS_H

#include <iostream>
#include <vector>
#include <string>
#include <sqlite3.h>

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "dbtool.h"
#include "dbtable.h"

//5p requirments, 5p autocomment, .... 1p requirments, 1p autocomment, csv comments
class DBRubrics : public DBTable
{
public:
    DBRubrics();
    DBRubrics(DBTool *db, std::string fileName);
    ~DBRubrics();

    //Create the sql data file
    void store_create_sql();
    void store_add_row_sql();

    //Add rubric
    int addRubric(std::string R5,  std::string R4,
                  std::string R3,  std::string R2,
                  std::string R1, std::string tags);
    bool select_all();

    std::vector<std::vector<std::string>> *rubrics;
};

//Callback for addComment
int cb_add_rubric(void  *data,
                   int    argc,
                   char **argv,
                   char **azColName);
//callback for rubrics
int cb_select_rubric(void  *data,
                     int    argc,
                     char **argv,
                     char **azColName);
#endif // DBRUBRICS_H
