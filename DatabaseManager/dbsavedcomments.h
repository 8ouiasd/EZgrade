#ifndef DBSAVEDCOMMENTS_H
#define DBSAVEDCOMMENTS_H

#include <vector>
#include <iostream>
#include <string>
#include <sqlite3.h>

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "dbtool.h"
#include "dbtable.h"
#include "searchtable.h"

#include <cstring>

class DBSavedComments : public DBTable
{
public:
    DBSavedComments();
    DBSavedComments(DBTool *db, std::string fileName);
    ~DBSavedComments();

    //Create the sql data file
    void store_create_sql();
    void store_add_row_sql();

    //add a new comment
    int addComment(std::string comment);

    //returns comments that have a similiarity of at least 1 will be returned in order of most similiar
    std::vector<std::string> searchComments(std::string searchTerms);

    bool select_all();

    std::vector<std::string> *comments;
    SearchTable *searchTable;
};

//Prints out all the data
int cb_select_allComments(void  *data,
                  int    argc,
                  char **argv,
                  char **azColName);

//Callback for addComment
int cb_add_saved_comment(void  *data,
                   int    argc,
                   char **argv,
                   char **azColName);
#endif // DBSAVEDCOMMENTS_H
