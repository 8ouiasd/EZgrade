#-------------------------------------------------
#
# Project created by QtCreator 2017-04-26T20:53:01
#
#-------------------------------------------------

#QT       += sql script testlib //--> what is 'script'? which will create errors
QT       += sql  testlib


SOURCES += dbtest.cpp

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

HEADERS += ../DatabaseManager/dbcomments.h
HEADERS += ../DatabaseManager/dbrubrics.h
HEADERS += ../DatabaseManager/dbsavedcomments.h
HEADERS += ../DatabaseManager/dbtable.h
HEADERS += ../DatabaseManager/dbtool.h
HEADERS += ../DatabaseManager/dbtoplevel.h
HEADERS += ../DatabaseManager/searchtable.h
HEADERS += ../DatabaseManager/dbstudentgrades.h

SOURCES += ../DatabaseManager/dbcomments.cpp
SOURCES += ../DatabaseManager/dbrubrics.cpp
SOURCES += ../DatabaseManager/dbsavedcomments.cpp
SOURCES += ../DatabaseManager/dbtable.cpp
SOURCES += ../DatabaseManager/dbtool.cpp
SOURCES += ../DatabaseManager/dbtoplevel.cpp
SOURCES += ../DatabaseManager/searchtable.cpp
SOURCES += ../DatabaseManager/dbstudentgrades.cpp

LIBS += -l sqlite3
