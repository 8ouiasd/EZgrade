/***************************************************************************/
//https://wiki.qt.io/Writing_Unit_Tests
//http://doc.qt.io/qt-5/qtest.html

//#include <QtWidgets>
#include <QtTest/QtTest>
#include <QFont>
#include <iostream>
//#include <QTextEdit>
#include <QString>
#include <QtTest>
#include <vector>
#include <iostream>
#include <string>
#include <sqlite3.h>
#include "../DatabaseManager/dbtool.h"
#include "../DatabaseManager/dbtable.h"
#include "../DatabaseManager/dbcomments.h"
#include "../DatabaseManager/dbrubrics.h"
#include "../DatabaseManager/dbsavedcomments.h"
#include "../DatabaseManager/dbstudentgrades.h"
#include "../DatabaseManager/searchtable.h"
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>

class DBTest : public QObject
{
    Q_OBJECT

public:
    DBTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    //Comments Testing
    void testAddComm();
    void testAmmendComm();
    void testGetAllComments();
    void testGetClassComments();
    void testGetClassComments2();
    //void testDeleteComm(); //This test fails
    //Rubrics Testing
    void testAddRubric();
    void testMultRubric();
    //Student Grade Testing
    void testAddGrading();
    //Search Table Testing
    void testStringSplit();
    void testGetRel();
    void testGetRelMul();
    void testGetRelDif();
};

/**
 * @brief DBTest::DBTest
 */
DBTest::DBTest()
{
}

/**
 * @brief DBTest::initTestCase
 */
void DBTest::initTestCase()
{
}

/**
 * @brief DBTest::cleanupTestCase
 */
void DBTest::cleanupTestCase()
{
}

//Comments Testing

/**
 * @brief DBTest::testCaseCode This test method tests the
 * contents of the comments database table to ensure what
 * is added is actually what is contained
 * @return nothing
 */
void DBTest::testAddComm() {
    DBTool *tool = new DBTool("test");
    DBComments comms(tool, "test1");
    comms.addComment("a","b","c");
    comms.select_all();
    std::string c;
    std::vector<std::vector<std::string>> *com = comms.comments;
    std::vector<std::string> theCom;
    for (int x = 0; x < com->size(); x++) {
        theCom = com->at(x);
        for (int y = 0; y < theCom.size(); y++) {
            c = theCom.at(y);
        }
    }
    //std::cout << "This is C: " << c << "\n";
    QVERIFY(c == "c");
    delete tool;
    //delete comms;
}

/**
 * @brief DBTest::testAmmendComm This method tests whether
 * a comment is properly ammended
 * @return nothing
 */
void DBTest::testAmmendComm() {
    DBTool *tool = new DBTool("test2");
    DBComments comms(tool, "test3");
    comms.addComment("a","b","c");
    for (int i = 0; i < comms.size(); i++) {
        comms.ammendComment("a",i,"d");
    }
    //comms.ammendComment("a",0,"d");
    comms.select_all();
    std::string c;
    std::vector<std::vector<std::string>> *com = comms.comments;
    std::vector<std::string> theCom;
    for (int x = 0; x < com->size(); x++) {
        theCom = com->at(x);
        for (int y = 0; y < theCom.size(); y++) {
            c = theCom.at(y);
            if (y == 4) {
                QVERIFY(c == "d");
            }
            //std::cout << "Variable C is: " << c << "\n";
        }
    }
    delete tool;
}

/**
 * @brief DBTest::testGetAllComments This test tests the
 * retrieval of all comments from our comments table
 * @return nothing
 */
void DBTest::testGetAllComments() {
    DBTool *tool1 = new DBTool("test4");
    DBComments commz(tool1, "test5");
    commz.addComment("x","y","z");
    commz.select_all();
    std::string str;
    std::vector<std::vector<std::string>> *comm = commz.comments;
    std::vector<std::string> theCom;
    for (int x = 0; x < comm->size(); x++) {
        theCom = comm->at(x);
        for (int y = 0; y < theCom.size(); y++) {
            str = theCom.at(y);
            if (y == 0) {
                QVERIFY(str == "x");
            }
            if (y == 1) {
                QVERIFY(str == "y");
            }
            if (y == 2) {
                QVERIFY(str == "z");
            }
        }
    }
    delete tool1;
}

/**
 * @brief DBTest::testGetClassComments This test tests the
 * retrieval of comments from a specific class from
 * our comments table
 * @return nothing
 */
void DBTest::testGetClassComments() {
    DBTool *tool2 = new DBTool("test6");
    DBComments commz(tool2, "test7");
    commz.addComment("h","i","j");
    commz.addComment("l","m","n");
    commz.getComments("h");
    std::string str;
    std::vector<std::vector<std::string>> *comm = commz.comments;
    std::vector<std::string> theCom;
    for (int x = 0; x < comm->size(); x++) {
        theCom = comm->at(x);
        for (int y = 0; y < theCom.size(); y++) {
            str = theCom.at(y);
            if (y == 0) {
                QVERIFY(str == "h");
            }
            if (y == 1) {
                QVERIFY(str == "i");
            }
            if (y == 2) {
                QVERIFY(str == "j");
            }
        }
    }
    delete tool2;
}

/**
 * @brief DBTest::testGetClassComments2 This test tests the
 * retrieval of comments from a specific class from
 * out comments table which is not the
 * first comment added
 * @return nothing
 */
void DBTest::testGetClassComments2() {
    DBTool *tool2 = new DBTool("test8");
    DBComments commz(tool2, "test9");
    commz.addComment("h","i","j");
    commz.addComment("l","m","n");
    commz.getComments("l");
    std::string str;
    std::vector<std::vector<std::string>> *comm = commz.comments;
    std::vector<std::string> theCom;
    for (int x = 0; x < comm->size(); x++) {
        theCom = comm->at(x);
        for (int y = 0; y < theCom.size(); y++) {
            str = theCom.at(y);
            if (y == 0) {
                QVERIFY(str == "l");
            }
            if (y == 1) {
                QVERIFY(str == "m");
            }
            if (y == 2) {
                QVERIFY(str == "n");
            }
        }
    }
    delete tool2;
}

/**
 * @brief DBTest::testDeleteComm This test tests the
 * deletion of a specifed comment from our comments table
 * @return nothing
 */
/*void DBTest::testDeleteComm() {
    DBTool *tool2 = new DBTool("test10");
    DBComments commz(tool2, "test11");
    commz.addComment("h","i","j");
    for (int i = 0; i < commz.size(); i++) {
        commz.deleteComment(i);
    }
    commz.getComments("h");
    std::string str;
    std::vector<std::vector<std::string>> *comm = commz.comments;
    std::vector<std::string> theCom;
    for (int x = 0; x < comm->size(); x++) {
        theCom = comm->at(x);
        for (int y = 0; y < theCom.size(); y++) {
            str = theCom.at(y);
            if (y == 0) {
                QVERIFY(str != "h");
            }
            if (y == 1) {
                QVERIFY(str != "i");
            }
            if (y == 2) {
                QVERIFY(str != "j");for (int x = 0; x < rub->size(); x++) {
            }
        }
    }
}*/

//Rubrics Testing

/**
 * @brief DBTest::testAddRubric This test tests the
 * addtion of a rubric to our rubrics table
 * @return nothing
 */
void DBTest::testAddRubric() {
    DBTool *tool = new DBTool("test12");
    DBRubrics rubs(tool, "test13");
    rubs.addRubric("a","b","c","d","e","tags");
    rubs.select_all();
    std::string rb;
    std::vector<std::vector<std::string>> *rub = rubs.rubrics;
    std::vector<std::string> theRub;
    for (int x = 0; x < rub->size(); x++) {
        theRub = rub->at(x);
        for (int y = 0; y < theRub.size(); y++) {
            rb = theRub.at(y);
            if (y == 0) {
                QVERIFY(rb == "a");
            }
            if (y == 1) {
                QVERIFY(rb == "b");
            }
            if (y == 2) {
                QVERIFY(rb == "c");
            }
            if (y == 3) {
                QVERIFY(rb == "d");
            }
            if (y == 4) {
                QVERIFY(rb == "e");
            }
            if (y == 5) {
                QVERIFY(rb == "tags");
            }
        }
    }
    delete tool;
}

/**
 * @brief DBTest::testMultRubric This test tests the
 * addition of multiple rubrics to
 * our rubrics table
 * @return nothing
 */
void DBTest::testMultRubric() {
    DBTool *tool = new DBTool("test14");
    DBRubrics rubs(tool, "test15");
    rubs.addRubric("v","w","x","y","z","tags");
    rubs.addRubric("h","i","j","k","l","tags");
    rubs.select_all();
    std::string rb;
    std::vector<std::vector<std::string>> *rub = rubs.rubrics;
    std::vector<std::string> theRub;
    for (int x = 0; x < rub->size(); x++) {
        theRub = rub->at(x);
        for (int y = 0; y < theRub.size(); y++) {
            rb = theRub.at(y);
            if (y == 0 && x == 0) {
                QVERIFY(rb == "v");
            }
            if (y == 1 && x == 0) {
                QVERIFY(rb == "w");
            }
            if (y == 2 && x == 0) {
                QVERIFY(rb == "x");
            }
            if (y == 3 && x == 0) {
                QVERIFY(rb == "y");
            }
            if (y == 4 && x == 0) {
                QVERIFY(rb == "z");
            }
            if (y == 5 && x == 0) {
                QVERIFY(rb == "tags");
            }
            if (y == 0 && x == 1) {
                QVERIFY(rb == "h");
            }
            if (y == 1 && x == 1) {
                QVERIFY(rb == "i");
            }
            if (y == 2 && x == 1) {
                QVERIFY(rb == "j");
            }
            if (y == 3 && x == 1) {
                QVERIFY(rb == "k");
            }
            if (y == 4 && x == 1) {
                QVERIFY(rb == "l");
            }
            if (y == 5 && x == 1) {
                QVERIFY(rb == "tags");
            }
        }
    }
    delete tool;
}

//Student Grade Testing

/**
 * @brief DBTest::testAddGrading This test tests the
 * addition of student grade data to
 * our grading table
 */
void DBTest::testAddGrading() {
    DBTool *tool = new DBTool("test16");
    DBStudentGrades stug(tool, "test17");
    stug.addGrading("class", 100);
    stug.select_all();
    std::string sg;
    std::vector<std::vector<std::string>> *stu = stug.grades;
    std::vector<std::string> theG;
    for (int x = 0; x < stu->size(); x++) {
        theG = stu->at(x);
        for (int y = 0; y < theG.size(); y++) {
            sg = theG.at(y);
            std::cout << "Info: " << sg << "\n";
            if (y == 1) {
                QVERIFY(sg == "100");
            }
        }
    }
    delete tool;
}

//Search Table Testing

/**
 * @brief DBTest::testStringSplit This test tests the
 * string spliter method of our search table
 * to check whether the string is broken
 * up and returned properly
 * @return nothing
 */
void DBTest::testStringSplit() {
    SearchTable src;
    src.stringSpliter("boardwalks");
    std::string res;
    std::vector<std::string> splitword;
    for (int x = 0; x < splitword.size(); x++) {
        res = splitword.at(x);
        QVERIFY(res == "board");
    }
}

/**
 * @brief DBTest::testGetRel This test tests the
 * comment searching method which should return the
 * ammount of times the single searched for string
 * is found within the table
 * @return nothing
 */
void DBTest::testGetRel() {
    SearchTable src1;
    src1.addComment("boardwalks");
    std::vector<int> rel;
    src1.getRelevence("boardwalks");
    int ammount;
    for (int x = 0; x < rel.size(); x++) {
        ammount = rel.at(x);
        QVERIFY(ammount == 1);
    }
}

/**
 * @brief DBTest::testGetRelMul This test tests the
 * comment searching method which should return the
 * ammount of times the single searched for string
 * is found within the table. In this case we test
 * multiple instances of the same comment
 * @return nothing
 */
void DBTest::testGetRelMul() {
    SearchTable src2;
    src2.addComment("boardwalks");
    src2.addComment("boardwalks");
    src2.addComment("boardwalks");
    std::vector<int> rel;
    src2.getRelevence("boardwalks");
    int ammount;
    for (int x = 0; x < rel.size(); x++) {
        ammount = rel.at(x);
        QVERIFY(ammount == 3);
    }
}

/**
 * @brief DBTest::testGetRelDif This test tests the
 * comment searching method which should return the
 * ammount of times the single searched for string
 * is found within the table. In this case we test
 * the return ammount of two different strings
 * @return nothing
 */
void DBTest::testGetRelDif() {
    SearchTable src3;
    src3.addComment("boardwalks");
    src3.addComment("sweet");
    std::vector<int> rel;
    src3.getRelevence("boardwalks");
    int ammount;
    for (int x = 0; x < rel.size(); x++) {
        ammount = rel.at(x);
        QVERIFY(ammount == 1);
    }
    src3.getRelevence("sweet");
    for (int x = 0; x < rel.size(); x++) {
        ammount = rel.at(x);
        QVERIFY(ammount == 1);
    }
}

QTEST_APPLESS_MAIN(DBTest)
#include "dbtest.moc"































