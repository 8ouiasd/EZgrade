/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//https://wiki.qt.io/Writing_Unit_Tests
//http://doc.qt.io/qt-5/qtest.html

#include <QtWidgets>
#include <QtTest/QtTest>
#include <QFont>
#include <QTextEdit>
#include <iostream>
#include <QMainWindow>
#include <QInputDialog> //Open a QInputDialog to get user input
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QDir>      //http://doc.qt.io/qt-4.8/qdir.html
#include <iostream>
#include <QFileSystemModel>
#include <QClipboard>
//#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include <QDialog>
#include <QtCore>
#include <QtGui>

class GUITest: public QObject
{
    Q_OBJECT

private slots:
    void testFontFamilySet();
    void testFontSizeSet();
    void testFontFixedPitch();
    void testSetForeground1();
    void testSetForeground2();
    void testFilterModels();
    void testFilterModels2();
    void testViewModels();
    void testViewModels2();
    void testTempPathString();
    void testTempPathString2();
    void testOutFileOpen();
    //void testOutFileOut();
};

/**
 * @brief GUITest::testFontFamilySet This test tests some of the
 * the visual characteristics
 * settings of the file tree view
 * @return nothing
 */
void GUITest::testFontFamilySet() {
    QFont javaViewerFont;
    javaViewerFont.setFamily("Courier");
    javaViewerFont.setFixedPitch(true);
    javaViewerFont.setPointSize(11);

    QCOMPARE(javaViewerFont.family(), QString("Courier"));
}

/**
 * @brief GUITest::testFontFixedPitch This test tests more of
 * the visual characteristics
 * settings of the file tree view
 * @return nothing
 */
void GUITest::testFontFixedPitch() {
    QFont javaViewerFont;
    javaViewerFont.setFamily("Courier");
    javaViewerFont.setFixedPitch(true);
    javaViewerFont.setPointSize(11);

    QVERIFY(javaViewerFont.fixedPitch() == true);
}

/**
 * @brief GUITest::testFontSizeSet This test tests more of
 * the visual characteristics
 * settings of the file tree view
 * @return nothing
 */
void GUITest::testFontSizeSet() {
    QFont javaViewerFont;
    javaViewerFont.setFamily("Courier");
    javaViewerFont.setFixedPitch(true);
    javaViewerFont.setPointSize(11);

    QCOMPARE(javaViewerFont.pointSize(), 11);
}

/**
 * @brief GUITest::testSetForeground1 This test tests the text
 * characteristic changes on certain button presses
 * @return nothing
 */
void GUITest::testSetForeground1() {
    QTextCharFormat colorFormat;
    colorFormat.setForeground(Qt::red);

    QVERIFY(colorFormat.foreground() != Qt::blue);
}

/**
 * @brief GUITest::testSetForeground2 This test tests the text
 * characteristic changes on certain button presses
 * @return nothing
 */
void GUITest::testSetForeground2() {
    QTextCharFormat colorFormat;
    colorFormat.setForeground(Qt::red);

    QVERIFY(colorFormat.foreground() == Qt::red);
}

/**
 * @brief GUITest::testFilterModels This test tests some of the
 * the file path
 * setting details
 * @return nothing
 */
void GUITest::testFilterModels() {
    QFileSystemModel fileModel;
    fileModel.setFilter(QDir::NoDotAndDotDot | QDir::Dirs);

    QCOMPARE(fileModel.filter(), QDir::NoDotAndDotDot | QDir::Dirs);
}

/**
 * @brief GUITest::testFilterModels2 This test tests more of the
 * the file path
 * setting details
 * @return nothing
 */
void GUITest::testFilterModels2() {
    QFileSystemModel fileModel;
    fileModel.setNameFilterDisables(false);

    QCOMPARE(fileModel.nameFilterDisables(), false);
}

/**
 * @brief GUITest::testViewModels This test tests some of the
 * setting details of the view model
 * @return nothing
 */
void GUITest::testViewModels() {
    QFileSystemModel viewModel;
    viewModel.setFilter(QDir::NoDotAndDotDot | QDir::Files);

    QCOMPARE(viewModel.filter(), QDir::NoDotAndDotDot | QDir::Files);
}

/**
 * @brief GUITest::testViewModels2 This test tests more of the
 * setting details of the view model
 * @return nothing
 */
void GUITest::testViewModels2() {
    QFileSystemModel viewModel;
    viewModel.setNameFilters(QStringList() << "*.java");

    QCOMPARE(viewModel.nameFilters(), QStringList() << "*.java");
}

/**
 * @brief GUITest::testTempPathString This test tests some of the user
 * input file path details
 * @return nothing
 */
void GUITest::testTempPathString() {
    QString userInputFilePath = NULL;
    QString tempPathString = userInputFilePath;

    QVERIFY(tempPathString == NULL);
}

/**
 * @brief GUITest::testTempPathString2 This test tests more of the user
 * input file path details
 * @return nothing
 */
void GUITest::testTempPathString2() {
    QString userInputFilePath = "Not Null";
    QString tempPathString = userInputFilePath;

    QVERIFY(tempPathString != NULL);
}

/**
 * @brief GUITest::testOutFileOpen This test tests some of the
 * file output settings
 * @return nothing
 */
void GUITest::testOutFileOpen() {
    QString fileName = "/Users/XG/Google Drive/LafaSync/CS205/out.txt";
    QFile file(fileName);

    QVERIFY(file.open(QFile::WriteOnly | QFile::Text) != true);
}


//======================
QTEST_MAIN(GUITest)
#include "guitest.moc"
























